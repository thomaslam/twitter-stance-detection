# Twitter Stance Detection

## Algorithms / ML models implemented
* Naive Bayes
* Perceptron
* Logistic Regression

## Algorithms / ML models to implement
* SVM
* Decision Trees
* More advanced Neural Network models
* Unsupervised Algorithms?
	* k-means